#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging

import codecs
import urllib2
import urllib
import requests
import json
import ast
import logging
import base64
from datetime import datetime, timedelta
requests.packages.urllib3.disable_warnings()
#import jsonToPut as jsonToPut

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

logger = logging.getLogger('pyCouchDb')

class CouchDbDriver():

	user = ''
	pwd = ''
	port =''
	url = ''
	db = ''

	# variabili che setto per la request
	urlToCall = ''
	urlType = ''
	urlPayload = {}
	urlParam = {}


	def __init__( self, workingDb ):

		self.user = 'admin'
		self.pwd = '78-AjhQ'
		self.port =':5984'
		self.url = 'http://rsis-zp-mongo1'
		self.db = workingDb

		# variabili che setto per la request
		self.urlToCall = ''
		self.urlType = ''
		self.urlPayload = {}
		self.urlParam = {}

	def getDbUp( self ):

		logger.debug(" ---------------------- INIZIO getDbUp  ------------------ ")

		self.urlToCall = '/_up'
		self.urlType = 'GET'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE getDbUp  ------------------ ")
		return result

	def getDbList( self ):

		logger.debug(" ---------------------- INIZIO getDbList  ------------------ ")

		self.urlToCall = '/_dbs/_all_docs'
		self.urlType = 'GET'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE getDbList  ------------------ ")
		return result


	def getDbDocs( self ):

		logger.debug(" ---------------------- INIZIO getDbDocs  ------------------ ")

		self.urlToCall = '/' +  self.db + '/_all_docs'
		self.urlType = 'GET'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE getDbDocs  ------------------ ")
		return result

	def getDbDocId( self, id ):

		logger.debug(" ---------------------- INIZIO getDbDocId  ------------------ ")

		self.urlToCall = '/' +  self.db + '/' + id
		self.urlType = 'GET'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE getDbDocId  ------------------ ")
		return result

	def getKeyframeDbDocId( self, id ):

		logger.debug(" ---------------------- INIZIO getKeyframeDbDocId  ------------------ ")
		result = ""
		tmpRes = {}
		tmpRes = self.getDbDocId( id )
		#logger.debug("tmpRes = " + str( tmpRes))
		
		if ( len(tmpRes) > 0 ) and 'keyframes' in tmpRes and len(tmpRes['keyframes']) > 0 :
			logger.debug( tmpRes['keyframes'][0] )
			result = tmpRes['keyframes'][0]

		logger.debug(" ---------------------- FINE getKeyframeDbDocId  ------------------ ")
		return result

	def deleteDbDocId( self, id ):

		logger.debug(" ---------------------- INIZIO deleteDbDocId  ------------------ ")

		# per prma cosa devo prendere la rev version dell item da cancellare
		rev = ''
		item = self.getDbDocId( id )
		if '_rev' in item:
			print ' item = ' , item 
			print item['_rev']
			rev = item['_rev']
		else:
			return None


		self.urlToCall = '/' +  self.db + '/' + id
		self.urlType = 'DELETE'
		self.urlPayload = {}
		self.urlParam = { 'rev':rev }

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE deleteDbDocId  ------------------ ")
		return result

	def createDb( self, db ):

		logger.debug(" ---------------------- INIZIO createDb ------------------ ")
		self.db = db

		self.urlToCall = '/' +  self.db 
		self.urlType = 'PUT'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE createDb  ------------------ ")
		return result


	def deleteDb( self ):

		logger.debug(" ---------------------- INIZIO deleteDb ------------------ ")

		self.urlToCall = '/' +  self.db 
		self.urlType = 'DELETE'
		self.urlPayload = {}
		self.urlParam = {}

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE deleteDb  ------------------ ")
		return result

	def putDbDocId( self, id, value ):

		logger.debug(" ---------------------- INIZIO putDbDocId  ------------------ ")

		self.urlToCall = '/' +  self.db + '/' + id
		self.urlType = 'PUT'
		self.urlPayload = value

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE putDbDocId  ------------------ ")
		return result



	def updateDbDoc( self, id, value ):

		logger.debug(" ---------------------- INIZIO updateDbDoc  ------------------ ")

		# per prma cosa devo prendere la rev version dell item da cancellare
		rev = ''
		item = self.getDbDocId( id )
		if '_rev' in item:
			print ' item = ' , item 
			print item['_rev']
			rev = item['_rev']
		else:
			return None


		self.urlToCall = '/' +  self.db + '/' + id
		self.urlType = 'PUT'
		self.urlPayload = value
		self.urlParam = { 'rev':rev }

		result  = self.sendRequest()
		logger.debug(" ---------------------- FINE updateDbDoc  ------------------ ")
		return result

	def sendRequest( self ):

		logger.debug(" ---------------------- INIZIO sendRequests  ------------------ ")

		finalUrl = self.url + self.port + '/' +  self.urlToCall
		logger.debug('URL: ' + finalUrl )

		base64string = base64.encodestring('%s:%s' % (self.user,self.pwd )).replace('\n', '')
		meta = self.urlPayload
		param = self.urlParam

		headers =  { 'Accept': 'application/json',
			     'Authorization': "Basic %s" % base64string }

		requ = requests.request( self.urlType, finalUrl ,headers=headers,params=self.urlParam,data=self.urlPayload)

		# bind your stream
		logger.debug('\r\n')
		result = json.loads(requ.text)
		logger.debug(json.dumps(result, indent=4))
		logger.debug(" ---------------------- FINE sendRequests  ------------------ ")

		# risetto i parametri
		self.urlToCall = ''
		self.urlType = ''
		self.urlPayload = {}
		self.urlParam = {}

		return result


	def insertLista( self, listaToPut ):

		logger.debug(" ---------------------- INIT insertLista  ------------------ ")
		count = 0
		listaError = {}
		
		for key, value in listaToPut.iteritems():
			count += 1 
			print key, value
			#aggiungo controllo che se chiave non esiste devo fare 
			esiste = self.getDbDocId( key )
			if 'error' in esiste:
				# una semplice put
				response =  self.putDbDocId( key, "{ \"keyframes\":" + json.dumps(value)+ "}" )
			else:
				# altrimenti una update
				# che cmq sovrascrive i valori precedenti
				# come richiesto da Vitali Ulisse
				response =  self.updateDbDoc( key, "{ \"keyframes\":" + json.dumps(value)+ "}" )
			if 'ok' in response:
				logger.debug('tutto ok')
			else:
				logger.debug('qualcosa non va')
				listaError[ key  ] = value
		logger.debug(" ---------------------- FINE insertLista  ------------------ ")
		return listaError





if __name__ == "__main__":
	
	#couchDb = CouchDbDriver('testnuovo')
	couchDb = CouchDbDriver('importkeyframe')
	#print couchDb.putDbDocId( 'EP972541', '{ "keyframes":["13562723"]}' )
	#result = couchDb.getDbDocId( 'CPTHE6')
	#logger.debug( 'result  = ' + str( result ))
	result = couchDb.getDbDocId( 'GP999696')
	#logger.debug( 'result  = ' + str( result ))
	#print couchDb.createDb ('importkeyframe')
	#print couchDb.getDbUp()
	#print couchDb.getDbList()
	#print couchDb.getDbDocs ()
	#print couchDb.deleteDb ()
	#print couchDb.createDb ('testnuovo')
	#print couchDb.getDbUp()
	#print couchDb.getDbList()
	#print couchDb.getDbDocs ()
	#print couchDb.insertLista({u'221971': [u'12245860'], u'GP943810': [u'12245863'], u'GP917618': [u'12245866',u'12245869']})
	#print couchDb.insertLista({u'CPTHE6666': [u'12245860',u'12245866',u'12245869']})
	exit(0)
	'''
	per importare il json dal postman
	fin = codecs.open('../tmpJson','r')
	lista_toput = json.load(fin)
	fin.close()
	print len(lista_toput)

	couchDb.insertLista(  lista_toput )
	print couchDb.getDbDocs ()
	exit(0)
	'''
	
	result = couchDb.getDbDocId( 'GP951842')
	logger.debug( 'result  = ' + str( result ))
	result = couchDb.getDbDocId( '1206615')
	logger.debug( 'result  = ' + str( result ))
	exit(0)

	#print jsonToPut.keyframeDict

	
	count = 0
	for key, value in lista_toput.iteritems():
		count += 1 
		print key, value
		response =  couchDb.putDbDocId( key, "{ \"keyframes\":" + json.dumps(value)+ "}" )
		if 'ok' in response:
			print 'tutto ok'
		else:
			print 'qualcosa non va'

	#print couchDb.getDbDocs ()
	result = couchDb.getDbDocId( 'TESTDI')
	logger.debug( 'result  = ' + str( result ))
	#result = couchDb.getDbDocId( '217973')
	#logger.debug( 'result  = ' + str( result ))
	exit(0)

	print couchDb.getDbUp()
	print couchDb.getDbList()
	print couchDb.getDbDocs ()
	#print couchDb.getImportKeyframeDocs()
	#print couchDb.getDbDocId( 'test', '204471')
	#print couchDb.getDbDocId( 'test', '20125')
	#print couchDb.getImportKeyframeDocId()
	print couchDb.putDbDocId( '20125', '{ "keyframes":["17", "18","19"]}' )
	print couchDb.putDbDocId( '666', '{ "keyframes":["17", "18","19"]}' )
	print couchDb.getDbDocId( '666')
	print couchDb.updateDbDoc( '666', '{ "keyframes":["9999","9999"]}' )
	print couchDb.getDbDocId( '666')

	#print couchDb.deleteDbDocId( 'test', '20125')
	#print couchDb.deleteDbDocId( 'test', '2012500')
	#print couchDb.deleteDbDocId( 'test', '20125')
	#print couchDb.getDbDocId( 'test', '20125')
