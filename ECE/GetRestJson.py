#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import urllib2
import urllib
import requests
import json
import ast
import logging
import time
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

logger = logging.getLogger('pyGetRestJson')

# fare una GET su http://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json
# mi permette di prendere il json 
# fare una POST su http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json
# mi permette di ributtare su il nuovo json

def PostJson( jsonToPost, url ):

        logger.debug(" ---------------------- INIT PostJson ------------------ " )
        result = {}
        try:

                meta =  jsonToPost

                param = { }


                headers =  { 'Content-type': 'application/json'}

                requ = requests.request('POST', url ,headers=headers,params=param,data=json.dumps(meta))


                #upload your video
                logger.debug(requ.status_code)
                logger.debug(requ.content)

                # get youtube id
                result = json.loads(requ.content)
                logger.debug(result)

	except Exception as e:
		logger.debug( 'PROBLEMI in PostJson : ' + str(e) )
                pass

        logger.debug(" ---------------------- FINE PostJson ------------------ " )
        return result



def GetJson( url ):

        logger.debug(" ---------------------- INIZIO GetJSON  ------------------ ")
        result = {}

        try:

                # https://graph.facebook.com/{article-id}
                url_per_info = url

                meta = {}
                headers = {}
                param = { }

                headers =  { 'Accept': 'application/json'}

                requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

                #print requ
                # bind your stream
                logger.debug('\r\n')
                result = json.loads( requ.text )
		if 'error' in result:
			raise Exception('Errore: ' + result['error'] )	
                logger.debug(json.dumps(result, indent=4))
                logger.debug(" ---------------------- FINE GetJSON  ------------------ ")

        except Exception as e:
		logger.error(' .. ')
                logger.error('ERROR: EXCEPT in GetJSON  = ' + str(e))
		return [ 0, str(e) ]
                pass
        return [1, result]




if __name__ == "__main__":

	keyframeJson = GetJson('https://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json')
	print keyframeJson
	exit(0)

	jsontoload = {u'GP765997': [2055479 ,2055473, 2055475], u'GP765666': [205566 ,665473, 205665] }
	print PostJson( jsontoload, 'http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json' )



