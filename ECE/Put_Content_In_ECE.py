import os 

# -*- coding: utf-8 -*-

import logging
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast
import requests
import re

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pyECE')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry


def Dump_ET_Link ( id, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(id)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	tree = ET.parse(result)
	tree.write(filename)

	return



def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	return

def Put_Link( link, filename ):
	
	logger.debug('------------------------ inizia Put_Link -------------- ')

        base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
        file = open(filename)
        dati = file.read()

        request = urllib2.Request(link, data=dati)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'PUT'
        result = urllib2.urlopen(request)

        #logger.debug(result.read())
        return
        xml = minidom.parse(result)
        fout = codecs.open(filename, 'w', 'utf-8')
        fout.write( xml.toxml() )
        fout.close()


        return

def Put_IdProd( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		link = prod_ece  + str(idx)
		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
        return True


def putId( idx, filename ):

	logger.debug('------------------------ inizia putId -------------- ')
	try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('putId : ' + str(idx) )
		#logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in putId : ' + str(e)
		return False

	logger.debug('------------------------ finisce putId -------------- ')
        return True



def Put_Id( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
        return True

def putIdFromStr( idx, xmlStr ):

	print'------------------------ inizia putIdFromStr -------------- '
	logger.debug('------------------------ inizia putIdFromStr -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

		dati = xmlStr
		dati = ( dati ).encode()
		link = os.environ['ECE_SERVER']  + str(idx)
		print link 
		logger.debug ( link )
		request = urllib2.Request(link)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		resultResponse = urllib2.urlopen(request)


		print resultResponse.status 
		logger.debug ( resultResponse.status )
		print resultResponse.headers 
		logger.debug ( resultResponse.headers )
		#print resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] 
		print resultResponse.reason 
		logger.debug ( resultResponse.reason )


	except Exception as e:
		print 'PROBLEMI in putIdFromStr : ' + str(e) 
		logger.debug ( 'PROBLEMI in putIdFromStr : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putIdFromStr -------------- ')
	return True




def PutJsonToRest( dati, path, nomefile ):

	logger.debug( '------------------------ inizia PutJsonToRest -------------- ' )
	#print 'dati : ' + str(dati) 
	try:
		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		stag_ece = 'https://sslstagingapp.rsi.ch/rsi-api/intlay/test/writefblands'
		prod_ece = 'http://www.rsi.ch/rsi-api/fblikesandshares/writefblands'
		

		
		#link = os.environ['ECE_SERVER']  + str(idx)
		link = os.environ['ECE_REST']


		logger.debug( 'path : ' + path )
		logger.debug( 'nomefile : ' + nomefile )
		headers = {
		'rsiauthentication': 'fb14nd5ru135',
		'pathfromheader': path,
		'nomefilefromheader': nomefile
		}    


		r = requests.post(link, headers=headers, json=dati)
		logger.debug( 'risultato post su :' + prod_ece + ' con autentication ' ) 
		logger.debug( r.status_code  )
		resultJson = r.json() 
		logger.debug( resultJson )
	
	except Exception as e:
		logger.debug( 'PROBLEMI in PutJsonToRest : ' + str(e) )
		return False

	logger.debug('------------------------ finisce PutJsonToRest -------------- ')
        return True



def Create_Img( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia Create_Img -------------- ' )

        result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	try:
		link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )
		logger.debug( 'link : ' + link )
		
		# apro il template file
		
		
		logger.debug( 'binaryUrl : ' + binaryUrl )
		logger.debug( 'titolo : ' + titolo )

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

	        dati=open( os.environ['IMG_RESOURCE_TEMPLATE'] ,'r').read()
		#dati = dati.replace('__CREATE_SECTION__', section )
		dati = dati.replace('__TITOLO_PICTURE__', titolo )
		dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

		logger.debug( ' data  = ' + dati )

		response = requests.request('POST', link, headers=headers, data=dati)

		logger.debug( 'response status: ' + str(response.status_code ))
		
		if 201 == response.status_code:
			if 'Location' in response.headers:
				result[0] = True
				result[1] = response.headers['Location'].split('/')[-1]
			else:
				result[1] = 'Error in risposta create Img'
				result[0] = False
			
	
	except Exception as e:
		logger.debug( 'PROBLEMI in Create_Img : ' + str(e) )
		return [False, '' ]

	logger.debug('------------------------ finisce Create_Img -------------- ')
        return result

def UploadBinary( file_path ):

        logger.debug(" ---------------------- INIT UploadBinary CLAD ------------------ " )
        result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	logger.debug( 'file path per upload : ' + file_path )
	fileHeader = file_path.split('/')[-1]

        try:
		http_client.HTTPConnection.debuglevel = 0

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		
		path_file = file_path
		logger.debug( 'path file : ' +  path_file)

		headers = { 'Authorization' : 'Basic %s' % base64string,
			'x-escenic-media-filename': fileHeader,
				'Content-Type' : 'image/jpeg' } 

		response = requests.request('POST', url, headers=headers, 
			      data=open(path_file,'r').read())

		logger.debug( 'response status: ' + str(response.status_code ))
		
		if 201 == response.status_code:
			if 'Location' in response.headers:
				result[0] = True
				result[1] = response.headers['Location']
				logger.debug(' response = ' + result[1])
	
			else:
				result[1] = 'Error in risposta create Img'
				result[0] = False
	

		http_client.HTTPConnection.debuglevel = 1

	except Exception as e:
		logger.error('ERROR: EXCEPT in UploadBinary  = ' + str(e))
		pass


        logger.debug(" ---------------------- FINE UploadBinary ------------------ " )
        return result

def creaNomeFile( contentType ):

	dateTimeObj = datetime.now()
	timestampStr = dateTimeObj.strftime("%d.%m.%Y_%H.%M.%S.%f")
	result = os.environ['FILE_LOG_DIR'] + '/' + contentType + '_' + timestampStr + '.xml'
	return result


def preparaLeadRel( eceId ):
	print '------------------------ INIT  preparaLeadRel -------------- ' 
	logger.debug ( '------------------------ INIT  preparaLeadRel -------------- ' )
	result = ''

	# apro il template opportuno

	with open( os.environ['RESOURCE_DIR'] + "template_lead_keyframe" + ".xml" , 'r' ) as fin:
	#fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_lead_keyframe" + ".xml" , 'r', 'utf-8' )
		eceTemplate = fin.read()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  eceId
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']
	rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB']
	rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL'

	#print rimpiazza

	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items())
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	result = text.strip()
	#print result
	#exit(0)
	print '------------------------ END  preparaLeadRel -------------- ' 
	logger.debug ( '------------------------ END  preparaLeadRel -------------- ' )
	return result



def preparaKeyframe( eceId ):
	print '------------------------ INIT  preparaKeyframe -------------- ' 
	logger.debug ( '------------------------ INIT  preparaKeyframe -------------- ' )
	result = ''

	# apro il template opportuno
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_keyframe" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  eceId
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']
	rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB']
	rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL'

	#print rimpiazza

	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items())
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	result = text.strip()
	#print result
	print '------------------------ END  preparaKeyframe -------------- ' 
	logger.debug ( '------------------------ END  preparaKeyframe -------------- ' )
	return result


def giraListaEditorial( entry , listaRel):
	print  '------------------------ INIT giraListaEditorial -------------- ' 
	# questa ritorna due lista : una per i lead e una per gli editorial

	editorial = []
	lead = []
	for res in listaRel:
		# in res ho la relazione  
		val = res.get(namespaces['metadata'] +'group')
		if 'EDITORIALKEYFRAMES' in val:
			#print res.attrib['href']
			editorial.append( res )
		else:
			lead.append( res )
	print lead
	print list(reversed(editorial))
	for rel in lead:
		#print  (ET.tostring(rel ))
		#exit(0)
		entry.append(rel)
	for rel in  list(reversed(editorial)):
		entry.append(rel)
			
	print  '------------------------ END giraListaEditorial -------------- ' 
	return entry

def rimuoviListaRelated( entry , rel):
	print  '------------------------ INIT rimuoviListaRelated -------------- ' 
	# questa rimuove dal xml la lista delle relazioni 

	list =  entry.findall(namespaces['atom'] + "link")
	for idx,lis in enumerate(list):
		print(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			print('rimuovo qualcosa')
			entry.remove( lis )
	print  '------------------------ END rimuoviListaRelated -------------- ' 
	return entry
		

def prendiListaRelated( entry , rel):

	result = []
	list =  entry.findall(namespaces['atom'] + "link")
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			result.append( lis )
	return result




def addEditorialKeyframe( assetEceId, xmlStr, keyframeId ):

	result = True

	try:
		# qui chiama la preparaKeyframe per avere il testo da attaccare in fondo 
		# all XML del eceId
		keyframeEceStr = preparaKeyframe( keyframeId )

		entry =  xmlStr.split('</entry>')[0]

		xmlStr = entry + keyframeEceStr.strip() + '</entry>'

		tree = ET.fromstring(xmlStr.encode('utf-8'))

		listaRelated = prendiListaRelated(  tree, 'related' ) 
		print listaRelated
		print list(reversed(listaRelated))
		tree = rimuoviListaRelated( tree , 'related' )
		tree = giraListaEditorial( tree , listaRelated ) 
		tree = ET.ElementTree(tree)

		nome_file = creaNomeFile( 'keyframe' )
		tree.write(nome_file)
		print nome_file
		result = putId(assetEceId, nome_file)

	except Exception as e:
		logger.warning( 'PROBLEMI in addEditorialKeyframe : ' + str(e) )
		logger.debug ( '------------------------ END addRelProgrammeVideo -------------- ' )
		return False

        print '------------------------ END addRelProgrammeVideo -------------- '  + str(assetEceId)
        logger.debug ( '------------------------ END addRelProgrammeVideo -------------- '  + str(assetEceId))
        return result


def addKeyframeToLead( assetEceId, xmlStr, keyframeId ):

	result = True

	try:
		# qui chiama la preparaKeyframe per avere il testo da attaccare in fondo 
		# all XML del eceId
		keyframeLeadStr = preparaLeadRel( keyframeId )

		entry =  xmlStr.split('</entry>')[0]

		xmlStr = entry + keyframeLeadStr.strip() + '</entry>'

		#print (xmlStr )

		tree = ET.fromstring(xmlStr.encode('utf-8'))
		tree = ET.ElementTree(tree)
		nome_file = creaNomeFile( 'keyframe' )
		tree.write(nome_file)
		result = putId(assetEceId, nome_file)

	except Exception as e:
		logger.warning( 'PROBLEMI in addKeyframeToLead : ' + str(e) )
		logger.debug ( '------------------------ END addKeyframeToLead -------------- ' )
		return False

	
        print '------------------------ END addKeyframeToLead -------------- ' + str(assetEceId) 
        logger.debug ( '------------------------ END addKeyframeToLead -------------- '  + str(assetEceId))
        return result








if __name__ == "__main__":

        os.environ['ECE_USER'] = 'TSMM'
        os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'

	

	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_BINARY'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary'
		
	UploadBinary( '/mnt/rsi_import/keyframe_traffic/archived/CP966669_P00000011_1.jpg' )

	dati  = { "chiave":"valore"}
	PutJsonToRest( dati  )
 
