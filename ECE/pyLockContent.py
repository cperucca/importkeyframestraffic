
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import urllib2
import urllib
import requests
import json
import ast
import logging
import time
import base64
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

logger = logging.getLogger('pyLockContent')

def GetLockId():
	
        logger.debug(" ---------------------- INIT GetLockId CLAD ------------------ " )
        result = ''
	# metto sezione 4 perche' e' la frontpage ed e' dove trovo il parametro che mi interessa
	url = os.environ['ECE_SECTION'] + '4'
	logger.debug( 'url per prendere lock number : ' + url )

        try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		
		headers = { 'Authorization' : 'Basic %s' % base64string } 

		response = requests.request('GET', url, headers=headers )

		logger.debug( 'response status: ' + str(response.status_code ))
		#logger.debug( 'response : ' + str(response.headers ))
		#logger.debug( 'response : ' + str( response.text ))
		for line in response.text.splitlines():
			logger.debug( line )
			if 'import.keyframe.traffic=' in line:
				result = line.split("import.keyframe.traffic=")[-1]
				return result

	except Exception as e:
		logger.error('ERROR: EXCEPT in GetLockId  = ' + str(e))


        logger.debug(" ---------------------- FINE GetLockId ------------------ " )
        return result

def LockContent( ):

        logger.debug(" ---------------------- INIT LockContent CLAD ------------------ " )
        result = [False, '']
	url = os.environ['LOCK_URL'] + GetLockId()
	logger.debug( 'url per lock : ' + url )

        try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		
		path_file = os.environ['LOCK_RESOURCE_DIR'] + 'lock.xml'
		logger.debug( 'path file : ' +  path_file)

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type' : 'application/atom+xml;type=entry' } 

		response = requests.request('POST', url, headers=headers, 
			      data=open(path_file,'r').read())

		logger.debug( 'response status: ' + str(response.status_code ))
		if 201 == response.status_code:
			result[0] = True
			result[1] = response.headers['Location']
	
			logger.debug( 'response : ' + str(response.headers ))

	except Exception as e:
		logger.error('ERROR: EXCEPT in LockContent  = ' + str(e))


        logger.debug(" ---------------------- FINE LockContent ------------------ " )
        return result




def iDLockContent( id ):

        logger.debug(" ---------------------- INIT LockContent CLAD ------------------ " )
        result = [False, '']
	url = os.environ['LOCK_URL'] + id
	logger.debug( 'url per lock : ' + url )

        try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		
		path_file = os.environ['LOCK_RESOURCE_DIR'] + 'lock.xml'
		logger.debug( 'path file : ' +  path_file)

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type' : 'application/atom+xml;type=entry' } 

		response = requests.request('POST', url, headers=headers, 
			      data=open(path_file,'r').read())

		logger.debug( 'response status: ' + str(response.status_code ))
		if 201 == response.status_code:
			result[0] = True
			result[1] = response.headers['Location']
	
			logger.debug( 'response : ' + str(response.headers ))

	except Exception as e:
		logger.error('ERROR: EXCEPT in LockContent  = ' + str(e))


        logger.debug(" ---------------------- FINE LockContent ------------------ " )
        return result


def ReleaseLock( url ):

        logger.debug(" ---------------------- INIT ReleaseLock ------------------ " )
        result = {}
	logger.debug( 'url per unlock : ' + url )

        try:

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

		headers = { 'Authorization' : 'Basic %s' % base64string } 

		response = requests.request('DELETE', url, headers=headers )
	
		logger.debug( 'response status: ' + str(response.status_code ))
		logger.debug( 'response : ' + str(response.headers ))

	except Exception as e:
		logger.error('ERROR: EXCEPT in ReleaseLock ' + str(e))


        logger.debug(" ---------------------- FINE ReleaseLock ------------------ " )
        return result

if __name__ == "__main__":
        os.environ['ECE_USER'] = 'TSMM'
        os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'



	os.environ['LOCK_URL'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/'
	os.environ['LOCK_ID'] = '11868353'
	os.environ['LOCK_URL'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/lock/article/'
	os.environ['LOCK_ID'] = '11868353'
	os.environ['LOCK_RESOURCE_DIR'] = '/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Resources/'
	os.environ['ECE_SECTION'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/'
	#result = GetLockId()
	#print result
	#exit(0)
	result = iDLockContent( os.environ['LOCK_ID'])
	print result
	if result[0]:
		ReleaseLock( result[1] )









