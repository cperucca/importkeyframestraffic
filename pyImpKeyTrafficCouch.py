
# -*- coding: utf-8 -*-


import logging
import logging.config

import os
import shutil
import json
import glob
import time

import ECE.GetRestJson as GetRestJson
import ECE.Put_Content_In_ECE as Put_Content_In_ECE
import ECE.pyLockContent as pyLockContent
import Resources.pySettaVars as pySettaVars
import DBApi.API_DB as API_DB
import COUCHDB.pyCouchDbDrive as pyCouchDbDrive


def SettaEnvironment( debug ):

        if (debug):
                print 'PRIMA DI SETTARE ENVIRONMENT'
        if (debug):
                print 'ENV : '
        if (debug):
                print os.environ

        if (debug):
                print '---------------------------------'

        Environment_Variables = {}

        print 'verifico il setting della variabile _ImpKeyTrafficEnv_ '
        if '_ImpKeyTrafficEnv_' in os.environ:
                if 'PRODUCTION' in os.environ['_ImpKeyTrafficEnv_']:
                        print 'variabile _ImpKeyTrafficEnv_ ha valore \'PRODUCTION\''
                        print 'setto ENV di PROD'
                        Environment_Variables = pySettaVars.PROD_Environment_Variables
                        pySettaVars.LOGS_CONFIG_FILE = pySettaVars.PROD_CONFIG_FILE
                else:
                        print 'variabile _ImpKeyTrafficEnv_ ha valore : ' + os.environ['_ImpKeyTrafficEnv_']
                        print 'diverso da \'PRODUCTION\''
                        print 'setto ENV di TEST'
                        Environment_Variables = pySettaVars.TEST_Environment_Variables
                        pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE

        else:
                print 'variabile _ImpKeyTrafficEnv_ non trovata: setto ENV di TEST'
                Environment_Variables = pySettaVars.TEST_Environment_Variables
                pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE


        for param in Environment_Variables.keys():
                #print "%20s %s" % (param,dict_env[param])
                os.environ[param] = Environment_Variables[ param ]


        if (debug):
                print 'DOPO AVER SETTATO ENVIRONMENT'
        if (debug):
                print 'ENV : '
        if (debug):
                print os.environ



def Prendi_Lista( workingDir ):

	result = {}
	
	lista_files = glob.glob( workingDir + '*.jpg' )
	lista_files = lista_files + glob.glob( workingDir + '*.JPG' )
	
	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	for lis in lista_files:
		nome_file = os.path.basename(lis)
		print nome_file
		componenti = nome_file.split('_')
		if len(componenti) > 2:
			legacyId = componenti[0]
			progressivo = componenti[-1]
			if legacyId in result:
				result[legacyId].append( lis )
			else:
				result[legacyId] = [ lis ]
			
	# qui restituisco un json tipo :
	# {'GP765997': ['/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP765997_P00000011_3.jpg', '/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP765997_P00000011_2.jpg'], 'GP765666': ['/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP765666_P00000011_2.jpg', '/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP765666_P00000011_1.jpg']}
	# nota il 3 che mi dice che probabilmente ne ho gia' importati 2 per quel legacy id

	return result



def Importa_Img( lista_immagini ):

	logger.debug('------------------------ INIT Importa_Img -------------- ')

	result = {}
	lista_rimaste = {}
	lista_archivio = []

	section = os.environ['IMPORT_SECTION']
	
	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	for legacy, img in lista_immagini.iteritems():
		# in img ho la lista di file_path che passo per upload del binary
		for file_path in img:
			logger.debug( ' file path = ' + file_path )
		
			binary = Put_Content_In_ECE.UploadBinary( file_path )
			if not binary[0] :
				# non son riuscito a fare la load del file ....
				# continuo senza cambiare img con id
				if legacy in lista_rimaste:
					lista_rimaste[legacy].append(file_path)
				else:
					lista_rimaste[legacy] = [file_path]
				continue
				
			else:

				# ho fatto upload binary e in binary[1] ho la location url del binary
				# da passare alla creazione del content picture
				result_crea = Put_Content_In_ECE.Create_Img( binary[1] , legacy, section )
				
				if not result_crea[0]:
					# con quel binary non sono riuscito a costruire un asset immagine
					if legacy in lista_rimaste:
						lista_rimaste[legacy].append(file_path)
					else:
						lista_rimaste[legacy] = [file_path]
					continue
				else:
					# ho creato img ed e andato tutto bene
					if legacy in result:
						result[legacy].append(result_crea[1])
					else:
						result[legacy] = [ result_crea[1] ]
				
					# e andato tutto bene posso spostarla nella directory di archiviazione
					# e la muovo in archived
					logger.debug( 'passo da sposta Img' )
					Sposta_Img( file_path )

	logger.debug( ' result = ' + str(result))
	logger.debug( ' rimaste = ' + str(lista_rimaste))
	logger.debug('------------------------ END Importa_Img -------------- ')

	return [ result , lista_rimaste ]

def Merge_Liste( lista_nuova, lista_old ):

	logger.debug('------------------------ INIT Merge_Liste -------------- ')
	result = {}
	

	try:
			
		result = lista_nuova.copy()
		
		for key, valueList in result.iteritems():
			if key in lista_old:
				# in lista_old[ key ] ho una lista di ECE ID 
				# da aggiungere alla result[ key ] 
				for lis in lista_old[ key ]:
					result[ key ].append( lis )
		for key, valueList in lista_old.iteritems():
			if key not in result:
				result[ key ] = valueList
				
	except Exception as e:
		logger.debug( 'PROBLEMI in Merge_Liste : ' + str(e) )
		return {}


	logger.debug( ' result = ' + str(result))
	logger.debug('------------------------ finisce Merge_Liste -------------- ')
        return result

def Sposta_Img(  file_path ):
	
	logger.debug('------------------------ INIT Sposta_Img -------------- ')
        try:
		file_name = os.path.basename( file_path )
		logger.debug( "mv " + file_path + " to -> " + os.environ['FTP_ARCHIVE_DIR'] + file_name )
		shutil.move( file_path,  os.environ['FTP_ARCHIVE_DIR'] + file_name )
		
	except Exception as e:
		logger.error('ERROR: EXCEPT in Sposta_Img  = ' + str(e))
		logger.error('ERROR: EXCEPT in Sposta_Img per immagine = ' + file_path)
		pass

	logger.debug('------------------------ END Sposta_Img -------------- ')


if __name__ == "__main__":

        print '-\n'
        print '------------ INIT ---------- pyImpKeyTraffic.py ---------------------'
        #print os.environ

        # questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
        SettaEnvironment( False )

        print '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n'
        logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE)
        logger = logging.getLogger('pyImpKeyTraffic')
        print ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n'

	'''
	lista_merge =  {'werwer': ['333333'] , 'GP765666': ['11892743']}
	lista_old =  {'GP765666': ['11866666', '123123123'], 'werwer' : ['11866666', 'wefwerwe']}
	Merge_Liste(lista_merge, lista_old )
	exit(0)
	print os.environ['REST_POST_URL'] 
	print GetRestJson.PostJson( lista_merge, os.environ['REST_POST_URL'] )
	exit(0)

	img_result = [ False, '' ]
	img_result = Put_Content_In_ECE.UploadBinary( '/mnt/rsi_import/keyframe_traffic/archived/CP966669_P00000011_1.jpg')
	if not img_result[0]:
		logger.error(' Non riesco ad uploadare la immagine: esco e lavoro la prossima volta ')
		exit(0)
	#img_bin = 'http://internal.publishing.production.rsi.ch/webservice/escenic/binary/-1/2019/6/18/15/6cc0413d-3d50-4453-b551-359afe397d5a.bin'

	crea_result = Put_Content_In_ECE.Create_Img( img_result[-1], 'titolo della Immagine', '5909' )
	print crea_result
	exit(0)

	'''

	# qui ci sara la lista delle immagini da importare prese dall ftp
	lista_immagini = {}
	# qui la lista delle immagini importate ( se esistono )
	lista_importate = {}
	# qui la lista delle immagini presenti nel DB ( se esistono )
	items_da_db = {}

	# qui devo prendere quelle eventualmente ancora presenti nel DB
	logger.debug('prendo i valori dal DB = ' +  os.environ['DB_NAME'])
	items_da_db = API_DB.Prendi_Items_Db( os.environ['DB_NAME'] )
	API_DB.Azzera_Db( os.environ['DB_NAME'] )
	logger.debug( items_da_db )

	# prendo la lista delle immagini che ci sono in 
	# FTP_DIR
	logger.debug(" FTP DIR = " + os.environ['FTP_DIR'] )
	lista_immagini = Prendi_Lista( os.environ['FTP_DIR'] )
	print lista_immagini
	# lista img nella forma 
	# {'GP865345': ['/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP865345_P00000011_1.jpg', '/home/perucccl/Webservices/STAGING/ImportKeyFramesTraffic/Ftp_Dir/GP865345_P00000011_2.jpg']}

	if len( lista_immagini ) < 1 and len( items_da_db ) < 1:
		logger.debug('Nessuna immagine da importare')
		exit(0)
	else:
		if len( lista_immagini ) > 0:
			# inserisco le immagini che ho trovato e che non ci sono ancora
			# nel DB come pictures in ECE
			# e in lista_rimaste lascio quelle ancora da fare

			[ lista_importate, lista_rimaste ] = Importa_Img( lista_immagini )
			# dove quelle importate hanno adesso la lista di ECE_IDs 
			# mentre le altre hanno ancora il path



	lista_completa = Merge_Liste( lista_importate, items_da_db )
	logger.debug( lista_completa )
	# lista_completa e nella forma : {'221971': ['12245860'], 'GP943810': ['12245863'], 'GP917618': ['12245866'], '158645': ['12245869']}
	# devo mandarla al Couch
	# lista_merge =  {u'221971': [u'12245860'], u'GP943810': [u'12245863'], u'GP917618': [u'12245866',u'12245869']}

	# qui al posto di teststaging ci vuole la var di sistema
	couchDb = pyCouchDbDrive.CouchDbDriver(os.environ['COUCH_DB'])

	result_rest = couchDb.insertLista( lista_completa )

        print '------------ END ---------- pyImpKeyTraffic.py ---------------------'
